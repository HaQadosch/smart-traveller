import { configureAxe } from 'jest-axe'

export const axe = configureAxe({
  impactLevels: ['critical']
})
