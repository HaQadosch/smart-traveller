import * as React from 'react'
import { screen, waitFor } from '@testing-library/react'
import '@testing-library/jest-dom'
import { render } from './test-utils'
import { MockedProvider, MockedResponse } from '@apollo/client/testing';
import { Home } from './Home'
import userEvent from '@testing-library/user-event'
import { FILTER_CITY, GET_CITY, GET_NEW_CITIES, FILTER_CITIES } from './queries';

const user = userEvent.setup()
const mocks: readonly MockedResponse<Record<string, any>>[] | undefined = [
  {
    request: {
      query: GET_NEW_CITIES,
      variables: {
        filter: { visited: false, wishlist: false }, limit: 3
      },
    },
    result: {
      data: {
        cities: { cities: [{ id: '1' }, { id: '2' }, { id: '3' }] },
      },
    },
  },
  {
    request: {
      query: GET_CITY,
      variables: {
        cityId: 0,
      },
    },
    result: {
      data: {
        city: { id: 0, name: "Moscow", country: "Russia", visited: false, wishlist: false },
      },
    },
  },
  {
    request: {
      query: FILTER_CITIES,
      variables: { "filter": { "name": "lon" }, "limit": 6, "offset": 0 },
    },
    result: {
      data: {
        "cities": {
          "cities": [{ "id": 1 }, { "id": 14 }, { "id": 163 }, { "id": 326 }, { "id": 377 }, { "id": 449 }],
          "total": 6
        }
      }
    },
  },
  {
    request: {
      query: FILTER_CITIES,
      variables: { "filter": { "name": "ru" }, "limit": 6, "offset": 0 },
    },
    result: {
      data: {
        "cities": {
          "cities": [{ "id": 35 }, { "id": 228 }, { "id": 277 }, { "id": 318 }, { "id": 323 }, { "id": 350 }],
          "total": 10
        }
      }
    }
  },
  {
    request: {
      query: FILTER_CITIES,
      variables: { "filter": { "name": "ru" }, "limit": 6, "offset": 6 },
    },
    result: {
      data: {
        "cities": {
          "cities": [{ "id": 417 }, { "id": 463 }, { "id": 480 }, { "id": 499 }],
          "total": 10
        }
      }
    }
  },
  {
    request: {
      query: FILTER_CITIES,
      variables: { "filter": { "name": "London" }, "limit": 6, "offset": 0 },
    },
    result: {
      data: {
        "cities": {
          "cities": [{ "id": 1 }],
          "total": 1
        }
      }
    },
  },
  {
    request: {
      query: FILTER_CITY,
      variables: { "filter": { "name": "London" } }
    },
    result: {
      data: {
        cities: {
          cities: [{ id: 1, name: "London", country: "United Kingdom", visited: true, wishlist: true }], total: 1
        }
      }
    }
    ,
  },
]

describe('<Home /> component', () => {
  it('renders the input', () => {
    render(<Home />)

    expect(screen.getByRole('searchbox')).toBeInTheDocument()
  })

  it('searches for a specific city with Enter key', async () => {
    render(<MockedProvider mocks={ mocks } addTypename={ false }><Home /></MockedProvider>)
    expect(screen.queryByText('1 Matching Results')).not.toBeInTheDocument()

    const target = screen.getByRole('searchbox')

    await user.pointer({ target, keys: '[MouseLeft]' })
    await user.keyboard('London')

    // if user don't press enter, the search is not triggered, so no result.
    expect(screen.queryByText('1 Matching Results')).not.toBeInTheDocument()

    await user.keyboard('{Enter}')

    expect(target).toHaveValue('London')

    await screen.findByText('1 Matching Results')
    // await expect(screen.findByRole('button', { name: /load more/i })).not.toBeInTheDocument()
    await waitFor(() => {
      expect(screen.getByText('1 Matching Results')).toBeInTheDocument()
      expect(screen.queryByRole('button', { name: /load more/i })).not.toBeInTheDocument()
    })
  })

  it('searches for a specific city with the glass icon', async () => {
    render(<MockedProvider mocks={ mocks } addTypename={ false }><Home /></MockedProvider>)
    expect(screen.queryByText('1 Matching Results')).not.toBeInTheDocument()

    const input = screen.getByRole('searchbox')
    const searchbtn = screen.getByRole('button', { name: /search/i })

    await user.pointer({ target: input, keys: '[MouseLeft]' })
    await user.keyboard('London')

    // if user don't click the icon, the search is not triggered, so no result.
    expect(screen.queryByText('1 Matching Results')).not.toBeInTheDocument()

    await user.pointer({ target: searchbtn, keys: '[MouseLeft]' })
    await waitFor(() => {
      expect(screen.getByText('1 Matching Results')).toBeInTheDocument()
      expect(screen.queryByRole('button', { name: /load more/i })).not.toBeInTheDocument()
    })
  })
})
