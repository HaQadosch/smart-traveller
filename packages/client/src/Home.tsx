import React, { useState } from 'react'
import type { FC } from 'react'
import { Container, InputRightElement, Input, Heading, InputGroup, IconButton, VStack, Divider, useToast, Spinner } from '@chakra-ui/react'
import { Search2Icon } from '@chakra-ui/icons'
import { gql, useLazyQuery } from '@apollo/client'
import { FILTER_CITIES, FILTER_CITY } from './queries'
import { CityRecommendation } from './CityRecommendation'
import { ICity } from './types'
import { MatchingResults } from './MatchingResults'

export const Home: FC = () => {
  const [city, setCity] = useState<string>('')
  const [cities, setCities] = useState<ICity[]>([])

  const [toastError, toastLoading] = useHomeToast()
  const [filterCity, { loading, error, data, fetchMore }] = useLazyQuery(FILTER_CITIES)

  if (error) {
    toastError()
    console.error({ error })
  }
  if (loading) {
    // toastLoading()
  } else {
    // console.log({ loading, error, data, cities })
  }

  return (
    <VStack spacing="8">
      <Heading as="h1">Smart traveller</Heading>
      <Container maxW="container.md">
        <InputGroup>
          <Input
            type='search'
            placeholder='Search for a city'
            value={ city }
            onChange={ evt => setCity(evt.target.value) }
            onKeyPress={ evt => { if (evt.key === 'Enter' && city.length >= 3) { handleInputAction() } } }
          />
          <InputRightElement children={ <IconButton aria-label="search" name='search' icon={ loading ? <Spinner /> : <Search2Icon /> } /> } onClick={ handleInputAction } />
          <datalist id="cityList">
            {/* make a filter search after 3 letters input 
              { data.map((id, name) => {
                <option value="name" key="id" />
              })}
                styled https://chakra-ui.com/docs/layout/stack#example
            */}
          </datalist>
        </InputGroup>

        {
          data?.cities.total === 0
            ? <Heading as="h2" py={ 4 }>No Matching Results</Heading>
            : null
        }
        {
          cities.length
            ? <MatchingResults total={ data?.cities.total } cities={ cities } loading={ loading } fetchMore={ loadMore } />
            : null
        }
        <Divider py={ 4 } borderBottomWidth={ 0 } />

        <CityRecommendation />
      </Container>
    </VStack>
  )

  function handleInputAction () {
    filterCity({ variables: { filter: { name: city }, limit: 6, offset: 0 } })
      .then(({ data }) => {
        setCities(data.cities.cities)
      })
  }

  function loadMore () {
    fetchMore({ variables: { offset: cities.length ?? 0 } })
      .then(
        ({ data }) => {
          setCities(prev => [...prev, ...data.cities.cities]);
        },
        error => {
          console.error({ fechMoreError: error });
        })
  }
}


function useHomeToast () {
  const toast = useToast()
  const errorToastID = 'error-toast'
  const infoToastID = 'info-toast'

  const toastError = () => {
    if (!toast.isActive(errorToastID)) {
      toast({
        title: 'Error fetching data',
        description: "AllMachingCities ",
        status: 'error',
        duration: 5000,
        isClosable: true,
      })
    }
  }

  const toastLoading = () => {
    if (!toast.isActive(infoToastID)) {
      toast({
        title: 'Fetching data',
        description: "AllMachingCities ",
        status: 'info',
        duration: 5000,
        isClosable: true,
      })
    }
  }

  return [toastError, toastLoading]
}