import * as React from 'react'
import { screen, waitFor } from '@testing-library/react'
import '@testing-library/jest-dom'
import { render } from './test-utils'
import { MockedProvider, MockedResponse } from '@apollo/client/testing';
import userEvent from '@testing-library/user-event'
import { FILTER_CITY, GET_CITY, GET_NEW_CITIES, FILTER_CITIES } from './queries';
import { MatchingResults } from './MatchingResults';

const user = userEvent.setup()
const mocks: readonly MockedResponse<Record<string, any>>[] | undefined = [
  {
    request: {
      query: GET_NEW_CITIES,
      variables: {
        filter: { visited: false, wishlist: false }, limit: 3
      },
    },
    result: {
      data: {
        cities: { cities: [{ id: '1' }, { id: '2' }, { id: '3' }] },
      },
    },
  },
  {
    request: {
      query: GET_CITY,
      variables: {
        cityId: 0,
      },
    },
    result: {
      data: {
        city: { id: 0, name: "Moscow", country: "Russia", visited: false, wishlist: false },
      },
    },
  },
  {
    request: {
      query: FILTER_CITIES,
      variables: { "filter": { "name": "lon" }, "limit": 6, "offset": 0 },
    },
    result: {
      data: {
        "cities": {
          "cities": [{ "id": 1 }, { "id": 14 }, { "id": 163 }, { "id": 326 }, { "id": 377 }, { "id": 449 }],
          "total": 6
        }
      }
    },
  },
  {
    request: {
      query: FILTER_CITIES,
      variables: { "filter": { "name": "ru" }, "limit": 6, "offset": 0 },
    },
    result: {
      data: {
        "cities": {
          "cities": [{ "id": 35 }, { "id": 228 }, { "id": 277 }, { "id": 318 }, { "id": 323 }, { "id": 350 }],
          "total": 10
        }
      }
    }
  },
  {
    request: {
      query: FILTER_CITIES,
      variables: { "filter": { "name": "ru" }, "limit": 6, "offset": 6 },
    },
    result: {
      data: {
        "cities": {
          "cities": [{ "id": 417 }, { "id": 463 }, { "id": 480 }, { "id": 499 }],
          "total": 10
        }
      }
    }
  },
  {
    request: {
      query: FILTER_CITIES,
      variables: { "filter": { "name": "London" }, "limit": 6, "offset": 0 },
    },
    result: {
      data: {
        "cities": {
          "cities": [{ "id": 1 }],
          "total": 1
        }
      }
    },
  },
  {
    request: {
      query: FILTER_CITY,
      variables: { "filter": { "name": "London" } }
    },
    result: {
      data: {
        cities: {
          cities: [{ id: 1, name: "London", country: "United Kingdom", visited: true, wishlist: true }], total: 1
        }
      }
    }
    ,
  },
]

describe('<MatchingResults /> component', () => {
  it('renders a card', async () => {
    render(<MatchingResults total={ 1 } cities={ [{ id: 0 }] } fetchMore={ () => { } } loading={ false } />)
    expect(screen.queryByText('1 Matching Results')).toBeInTheDocument()

    await screen.findByRole('img', { name: /Moscow/i })
    expect(screen.queryByRole('button', { name: /load/i })).not.toBeInTheDocument()
  })

  it('displays _load more_ button when more than 6 matching results are found', async () => {
    render(<MatchingResults total={ 7 } cities={ [{ id: 0 }] } fetchMore={ () => { } } loading={ false } />)
    expect(screen.queryByText('7 Matching Results')).toBeInTheDocument()

    await screen.findByRole('img', { name: /Moscow/i })
    expect(screen.queryByRole('button', { name: /load/i })).toBeInTheDocument()
  })

  it('does not display _load more_ button when all the matching results are displayed', async () => {
    render(<MatchingResults total={ 7 } cities={ [{ id: 0 }, { id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }, { id: 6 }] } fetchMore={ () => { } } loading={ false } />)
    expect(screen.queryByText('7 Matching Results')).toBeInTheDocument()

    await screen.findByRole('img', { name: /Moscow/i })
    expect(screen.queryByRole('button', { name: /load/i })).not.toBeInTheDocument()
  })

  it('clicking load more button triggers fetchMore', async () => {
    const mockFetch = jest.fn()
    render(<MatchingResults total={ 7 } cities={ [{ id: 0 }] } fetchMore={ mockFetch } loading={ false } />)
    expect(screen.queryByText('7 Matching Results')).toBeInTheDocument()

    await screen.findByRole('img', { name: /Moscow/i })
    const loadMoreBtn = screen.getByRole('button', { name: /load/i })
    expect(loadMoreBtn).toBeInTheDocument()

    await user.pointer({ target: loadMoreBtn, keys: '[MouseLeft]' })

    await waitFor(() => {
      expect(mockFetch).toHaveBeenCalled()
    })
  })
})
