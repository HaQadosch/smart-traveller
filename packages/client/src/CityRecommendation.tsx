import * as React from 'react'
import { Heading, SimpleGrid } from '@chakra-ui/react'
import { useLazyQuery } from '@apollo/client'
import { CityCard } from './cityCard'
import { GET_NEW_CITIES } from './queries'
import type { ICity } from './types'

export const CityRecommendation: React.FC = () => {
  const [getNewCities, { loading, error, data }] = useLazyQuery(GET_NEW_CITIES, { variables: { filter: { visited: false, wishlist: false }, limit: 3 } })

  React.useEffect(() => {
    getNewCities()
  }, [])

  if (error) {
    console.error({ getNewCitiesError: error })
    return <CityCard id={ 0 } /> // Should be an errorCards
  }
  if (loading) {
    return <CityCard id={ 0 } /> // Should be loadingCards
  } else {

    return (
      <>
        <Heading as="h2" py={ 4 } >Recommendation</Heading>
        <SimpleGrid columns={ [2, null, 3] } spacing='40px'>
          {
            ((data?.cities.cities ?? []) as ICity[]).map(({ id }) => (
              <CityCard key={ id } id={ id } />
            ))
          }
        </SimpleGrid>
      </>
    )
  }
}


