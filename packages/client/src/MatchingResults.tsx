import React from 'react';
import { Heading, Divider, SimpleGrid, Button } from '@chakra-ui/react';
import { AddIcon } from '@chakra-ui/icons';
import { ICity } from './types';
import { CityCard } from './cityCard';

interface IMatchingResults {
  total: number
  cities: Array<{ id: ICity["id"] }>
  fetchMore: () => void
  loading: boolean
}
export const MatchingResults: React.FC<IMatchingResults> = ({ total, cities, fetchMore, loading }) => {
  return (<>
    <Divider py={ 4 } borderBottomWidth={ 0 } />
    <Heading as="h2" py={ 4 }>{ total } Matching Results</Heading>
    <SimpleGrid columns={ [2, null, 3] } spacing='40px'>
      { cities.map(({ id }) => (
        <CityCard key={ id } id={ id } />
      )) }
    </SimpleGrid>
    { cities.length < total
      ? <Button
        mt={ 4 }
        rightIcon={ <AddIcon /> }
        isLoading={ loading }
        loadingText='Loading'
        colorScheme='teal'
        variant='outline'
        spinnerPlacement='end'
        onClick={ fetchMore }
      >Load more
      </Button>
      : null }
  </>)
}
