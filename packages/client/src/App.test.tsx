import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { screen } from '@testing-library/react'
import { render } from './test-utils'
import { toHaveNoViolations } from 'jest-axe'
import { axe } from '../axe-helper'
import { App } from './App'

expect.extend(toHaveNoViolations)

describe('<App /> component', () => {
  it('renders the Header content', () => {
    render(
      <BrowserRouter>
        <App />
      </BrowserRouter>
    )
    const HeadingComponent = screen.getByText(/^Smart traveller$/i)
    expect(HeadingComponent).toBeInTheDocument()
  })


  it('should have basic a11y', async () => {
    const { container } = render(
      <BrowserRouter>
        <App />
      </BrowserRouter>
    )

    const results = await axe(container)

    expect(results).toHaveNoViolations()
  })
})

