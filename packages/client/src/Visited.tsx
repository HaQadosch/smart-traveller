import * as React from 'react';
import { Container, Heading } from '@chakra-ui/react'
import { SimpleGrid } from '@chakra-ui/react';
import { useQuery } from '@apollo/client'
import { CityCard } from './cityCard';
import { GET_VISITED_CITIES } from './queries';
import { ICity } from './types';

export const Visited: React.FC = () => (
  <>
    <Heading as="h1">Visited</Heading>
    <Container centerContent maxW="container.md" flexDir="row" py={ 4 }>
      <CityVisited />
    </Container>
  </>
)

export const CityVisited: React.FC = () => {
  const { loading, error, data } = useQuery(GET_VISITED_CITIES)
  if (error) {
    console.log({ toast: error })
    return <CityCard id={ 0 } />
  }
  if (loading) {
    // console.log({ toast: 'loading' })
    return <CityCard id={ 0 } />
  } else {
    const { cities: { cities, total } } = data
    // console.log({ loading, error, total, visited: cities })
    return (
      <SimpleGrid columns={ [2, null, 3] } spacing='40px'>
        {
          (cities as ICity[]).map(({ id }) => (
            <CityCard key={ id } id={ id } />
          ))
        }
      </SimpleGrid>
    )
  }
}
