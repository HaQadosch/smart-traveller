import { DocumentNode, gql } from "@apollo/client"

// wishlist
export const GET_VISITED_CITIES = gql`
  query GetVisitedCities {
    cities(filter: { visited: true }) {
      total
      cities {
        id
      }
    }
  }
`

export const GET_WISHLISTED_CITIES = gql`
  query GetWishlistedCities {
    cities(filter: { wishlist: true }) {
      total
      cities {
        id
      }
    }
  }
`


/**
{
  "filter": {
    "visited": null,
    "wishlist": null
  },
  "limit": null
}
 */
export const GET_NEW_CITIES = gql`
  query GetNewCities($filter: CitiesFilters, $limit: Int) {
    cities(filter: $filter, limit: $limit) {
      cities {
        id
      }
    }
  }
`


export const UPDATE_CITY = gql`
  mutation UpdateCity ($input: CitiesMutationInput) {
    updateCity(input: $input) {
      id
      name
      country
      visited
      wishlist
    }
  }
`

export const GET_CITY = gql`
  query GetCity($cityId: Int!) {
    city(id: $cityId) {
      id
      name
      country
      visited
      wishlist
    }
  }
`

/**
{
  "filter": {
    "name": null
  }
}
 */
export const FILTER_CITY = gql`
  query FilterCity($filter: CitiesFilters) {
    cities(filter: $filter) {
      cities {
        id
        name
        country
        visited
        wishlist
      }
      total
    }
  }
`

/**
{
  "limit": 10,
  "filter": {
    "country": "lo",
    "name": "lo"
  },
  "offset": 0
}
 */

export const FILTER_CITIES = gql`
  query Cities($limit: Int, $filter: CitiesFilters, $offset: Int) {
  cities(limit: $limit, filter: $filter, offset: $offset) {
    cities {
      id
    }
    total
  }
}
`