import * as React from "react"
import { useMutation, useQuery } from "@apollo/client"
import { AddIcon, CheckIcon } from "@chakra-ui/icons"
import { Badge, Box, IconButton, Image, Spinner } from "@chakra-ui/react"
import { GET_CITY, GET_VISITED_CITIES, GET_WISHLISTED_CITIES, UPDATE_CITY } from "./queries"
import { faker } from '@faker-js/faker'
import { ICity } from "./types"

export const CityCard: React.FC<{ id: ICity["id"] }> = ({ id }) => {
  const { initLoading, updateCity, data: { city, loading, error } } = useUpdateCity(id)

  const property = () => ({
    imageUrl: 'https://placeimg.com/640/480/arch',
    imageAlt: `${ city()?.name } in ${ city()?.country }`,
    id: id,
    title: `${ city()?.name } in ${ city()?.country }`,
  })


  return (
    <Box maxW='sm' borderWidth='1px' borderRadius='lg' overflow='hidden'>
      {
        initLoading
          ? <Spinner />
          : (<>
            <Box pos="relative">
              <Image src={ property().imageUrl } alt={ property().imageAlt } />
              {
                loading
                  ? <IconButton
                    pos="absolute" top="0" right="0"
                    aria-label='toggle wishlist'
                    icon={ <Spinner /> }
                  />
                  : (
                    <>
                      <IconButton
                        pos="absolute" top="0" right="0"
                        aria-label='toggle visited'
                        opacity={ 1 }
                        onClick={ () => updateCity?.({ variables: { input: { id, wishlist: (city() as ICity).wishlist, visited: !(city() as ICity).visited } } }) }
                        icon={ <CheckIcon color={ city()?.visited ? 'teal.500' : 'gray.300' } /> }
                      />
                      <IconButton
                        pos="absolute" top="0" left="0"
                        aria-label='toggle wishlist'
                        opacity={ 1 }
                        onClick={ () => updateCity?.({ variables: { input: { id, wishlist: !(city() as ICity).wishlist, visited: (city() as ICity).visited } } }) }
                        icon={ <AddIcon color={ city()?.wishlist ? 'teal.500' : 'gray.300' } /> }
                      />
                    </>
                  )
              }
            </Box>
            <Box p='6'>
              <Box display='flex' alignItems='baseline'>
                {
                  city()?.visited
                    ? <Badge borderRadius='full' px='2' colorScheme='teal'>Visited</Badge>
                    : null
                }
                {
                  city()?.wishlist
                    ? <Badge borderRadius='full' px='2' colorScheme='teal' mx={ 2 }>Wishlist</Badge>
                    : null
                }
              </Box>
              <Box display='flex' alignItems='baseline'>
                <Box
                  color='gray.500'
                  fontWeight='semibold'
                  letterSpacing='wide'
                  fontSize='xs'
                  textTransform='uppercase'
                  ml='2'
                >
                  id { property().id }
                </Box>
              </Box>

              <Box
                mt='1'
                fontWeight='semibold'
                as='h4'
                lineHeight='tight'
              >
                { property().title }
              </Box>

            </Box>
          </>)
      }
    </Box>
  )
}

const useUpdateCity = (id: number) => {
  const { loading: reqLoading, error: reqError, data: reqData } = useQuery<{ city: ICity }>(GET_CITY, { variables: { cityId: id } })
  const [updateCity, { data, loading, error }] = useMutation<{ city: ICity }>(UPDATE_CITY, {
    refetchQueries: [
      { query: GET_VISITED_CITIES },
      { query: GET_WISHLISTED_CITIES },
    ]
  })

  const city = () => data?.city ?? reqData?.city

  return { initLoading: reqLoading, updateCity, data: { city, loading, error } }
}